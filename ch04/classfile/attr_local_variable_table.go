package classfile

type LocalVariableTableAttribute struct {
	LocalVariableTable []*LocalVariableTableEntry
}

type LocalVariableTableEntry struct {
	StartPc         uint16
	Length          uint16
	NameIndex       uint16
	DescriptorIndex uint16
	Index           uint16
}

func (self *LocalVariableTableAttribute) readInfo(reader *ClassReader) {
	localVariableTableLength := reader.readUint16()
	self.LocalVariableTable = make([]*LocalVariableTableEntry, localVariableTableLength)
	for i := range self.LocalVariableTable {
		self.LocalVariableTable[i] = &LocalVariableTableEntry{
			StartPc: reader.readUint16(),
			Length: reader.readUint16(),
			NameIndex: reader.readUint16(),
			DescriptorIndex: reader.readUint16(),
			Index: reader.readUint16(),
		}
	}
}