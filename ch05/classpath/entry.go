package classpath

import (
	"os"
	"strings"
)

//静态不变的变量，系统的目录分割符
const pathListSeparator = string(os.PathSeparator)

type Entry interface {
	readClass(className string) ([]byte, Entry, error)
	String() string
}

func newEntry(path string) Entry {
	if strings.Contains(path, pathListSeparator) {
		return newCompositeEntry(path)
	}
	if strings.HasSuffix(path, "*") {
		return newWildcardEntry(path)
	}
	if strings.HasSuffix(path, ".jar") || strings.HasPrefix(path, ".JAR") ||
		strings.HasSuffix(path, ".zip") || strings.HasPrefix(path, ".ZIP") {
		return newZipEntry(path)
	}
	return newDirEntry(path)
}
