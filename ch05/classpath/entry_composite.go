package classpath

import (
	"fmt"
	"strings"
)

type CompositeEntry []Entry

func newCompositeEntry(pathList string) CompositeEntry {
	//原来的代码，感觉append会有影响，而且不好看
	//compositeEntry := []Entry{}
	//for _, path := range strings.Split(pathList, pathListSeparator) {
	//	entry := newEntry(path)
	//	compositeEntry = append(compositeEntry, entry)
	//}
	//return compositeEntry

	paths := strings.Split(pathList, pathListSeparator)
	compositeEntry := make([]Entry, len(paths))
	for i, path := range paths {
		entry := newEntry(path)
		compositeEntry[i] = entry
	}
	return compositeEntry
}

func (self CompositeEntry) readClass(className string) ([]byte, Entry, error) {
	for _, entry := range self {
		data, from, err := entry.readClass(className)
		if err == nil {
			return data, from, nil
		}
	}
	return nil, nil, fmt.Errorf("class not found: %s", className)
}

func (self CompositeEntry) String() string {
	strs := make([]string, len(self))
	for i, entry := range self {
		strs[i] = entry.String()
	}
	return strings.Join(strs, pathListSeparator)
}
