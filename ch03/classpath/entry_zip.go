package classpath

import (
	"archive/zip"
	"fmt"
	"io/ioutil"
	"path/filepath"
)

type ZipEntry struct {
	absPath string
}

func newZipEntry(path string) *ZipEntry {
	absPath, err := filepath.Abs(path)
	if err != nil {
		panic(absPath)
	}
	return &ZipEntry{absPath}
}

func (self *ZipEntry) String() string {
	return self.absPath
}

func (self *ZipEntry) readClass(className string) ([]byte, Entry, error) {
	r, err := zip.OpenReader(self.absPath)
	if err != nil {
		return nil,nil,err
	}
	defer r.Close()

	//遍历zip文件
	for _,f := range r.File {
		if f.Name == className {
			rc, err := f.Open()
			if err != nil {
				return nil,nil,err
			}
			//本来不应该在循环中使用defer， 但是一旦进入了这个if条件，就会必须有return的情况，所有这里的defer不会造成资源争夺
			defer rc.Close()

			data, err := ioutil.ReadAll(rc)
			if err != nil {
				return nil, nil, err
			}
			return data,self,nil
		}
	}
	return nil, nil, fmt.Errorf("class not found: %s",className)
}