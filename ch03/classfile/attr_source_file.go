package classfile

type SourceFileAttribute struct {
	cp ConstantPool
	sourceFileInfex uint16
}

func (self *SourceFileAttribute) readInfo(reader *ClassReader) {
	self.sourceFileInfex = reader.readUint16()
}

func (self *SourceFileAttribute) FileName() string {
	return self.cp.getUtf8(self.sourceFileInfex)
}
